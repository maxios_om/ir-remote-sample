

#include <IRremote.h>



int RECV_PIN = 11;
int RELAY_PIN = 13;
boolean isRelayOn = false;
IRrecv irrecv(RECV_PIN);
decode_results results;
int irEventCounter = 0;

/////////////////////////////////////////////////////////////////////////////
// OOPS not structs any more class is divine choice  

class OK_IRCommand 
{

public:
  String commandName;
  String commandCode; 


  OK_IRCommand() 
  {

    commandName = "";
    commandCode = "";
  }

  OK_IRCommand(String cName, String cCode) 
  {

    commandName = cName;
    commandCode = cCode;
  }

};

//CMD table aka array with known commands 

OK_IRCommand commandTable[] = 
{ 
  OK_IRCommand("OFF_ON", "1FE48B7"), OK_IRCommand("MODE", "1FE58A7"), OK_IRCommand("MUTE", "1FE7887"), OK_IRCommand("PAUSE", "1FE807F"), OK_IRCommand("BACK", "1FE40BF"), 
  OK_IRCommand("FORWARD", "1FEC03F"), OK_IRCommand("EQ", "1FE20DF"), OK_IRCommand("VAL-", "1FEA05F"), OK_IRCommand("VAL+", "1FE609F"), OK_IRCommand("REPAT", "1FE10EF"), 
  OK_IRCommand("U/SD", "1FE906F"), 
  OK_IRCommand("0", "1FEE01F"), OK_IRCommand("1", "1FE50AF"), OK_IRCommand("2", "1FED827"), OK_IRCommand("3", "1FE906F"), OK_IRCommand("4", "1FE30CF"),
  OK_IRCommand("5", "1FEB04F"), OK_IRCommand("6", "1FE708F"), OK_IRCommand("7", "1FE00FF"), OK_IRCommand("8", "1FEF00F"), OK_IRCommand("9", "1FE9867"),
  OK_IRCommand("REPEET_CODE", "FFFFFFFF")
};

/////////////////////////////////////////////////////////////////////////////
// by adding throw compiler ignores named type error some how :) i found this on stack so dont know much how this thing works out 
  OK_IRCommand commandForCode(String key) throw() 
  {

    int sizeOFArray = sizeof(commandTable);
    OK_IRCommand cmd = OK_IRCommand();

    for (int index = 0 ; index < sizeOFArray ; index ++) 
    {

      cmd = commandTable[index];
      if(cmd.commandCode.equalsIgnoreCase(key)) 
      {

        return cmd;
        break;
      }

    }

    return cmd;

  }

// now execute cmd 
// This func should be generic i know but this is testing version 

boolean toggleRelay(OK_IRCommand cmd) throw()
{

  // if got repet code then just ignore it ;)
  if(cmd.commandName.equalsIgnoreCase("REPEET_CODE")) 
  {
    return isRelayOn;
  }

  if(cmd.commandName.equalsIgnoreCase("OFF_ON")) 
  {

    isRelayOn = !isRelayOn;
    digitalWrite(RELAY_PIN , isRelayOn); 

  }

  return isRelayOn;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void setup()
{
  Serial.begin(9600);
  irrecv.enableIRIn(); // Start the receiver
  pinMode(RELAY_PIN , OUTPUT); // set our relay pin as op 
  digitalWrite(RELAY_PIN , isRelayOn); // set it according to the relay status  
}

void loop() 
{

  if (irrecv.decode(&results)) 
  {
    irEventCounter ++;
    String hexString = String(results.value, HEX);

    //    Serial.println(results.value, HEX);
    OK_IRCommand cmd = commandForCode(hexString);
    boolean relayStatus = toggleRelay(cmd);
    Serial.println("commandName : ");
    Serial.println(cmd.commandName);
    Serial.println("\n");
    Serial.println("commandCode : ");
    Serial.println(cmd.commandCode);
    Serial.println("\n");
    Serial.println("Relay Status : ");
    Serial.println(relayStatus);
    Serial.println("\n");
    
    
    irrecv.resume(); // Receive the next value
  }
  delay(100);
}




